<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Jenis_barang extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("jenis_barang_model");
	}
	
	public function index()
	
	{
		$this->listjenisBarang();
	}
	
	public function listjenisBarang()
	
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		$this->load->view('HomeJenisBarang', $data);
	}


	public function input()
	{
		$data['data_barang'] = $this->barang_model->tampilDataBarang();
		
		if (!empty($_REQUEST)) {
			$m_jenisbarang = $this->jenis_barang_model;
			$m_jenisbarang->save();
			redirect("Jenis_barang/index", "refresh"); 
		}
		
		
		$this->load->view('InputJenisBarang', $data);
	}
	
	   public function detail_jenis_barang($kode_jenis)
	   {
			$data['detail_jenis_barang']	= $this->jenis_barang_model->detail($kode_jenis);
			$this->load->view('detail_jenis_barang', $data);   
	   }








}
